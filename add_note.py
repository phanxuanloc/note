import uuid
import os
import dashboard
import helper
import main as m

def addNote():
    nameFile = str(uuid.uuid1().hex) + ".txt"

    f = open('file/' + nameFile, "x")

    content = helper.inputContent()

    if content != "":
        f.write(content)
    else:
        f.close()
        os.remove('file/' + nameFile)
    
def addNoteMain():
    print()
    addNote()
    print()
    m.main()
