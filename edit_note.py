import os
import helper as hp
import main as m

def chooseUser():
    return int(input("Vui lòng nhập số thứ tự file muốn sửa hoặc quay lại:"))

def editNote(fileName):
    f = open('file/' + fileName, 'w')
    content = hp.inputContent()

    if content != "":
        f.write(content)
        f.close()
    else:
        f.close()
        os.remove('file/' + fileName)
    
    m.main()
    
def editNoteMain():
    print()
    print("***** MÀN HÌNH SỬA GHI CHÚ *****")
    print()

    choose = 0

    hp.menu()
    hp.space()

    hp.showList()

    fileNote = hp.fileNote()

    while True:
        try:
            choose = chooseUser()
        except:
            continue
        if choose < 0 or choose > len(fileNote):
            continue
        else:
            break

    if choose == 0:
        m.main()
    else:
        editNote(fileNote[choose - 1])
    


