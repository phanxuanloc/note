import os
import list_note as ln
import helper as hp
import main as m

def fileNote():
    files = os.listdir('file/')

    return files

def chooseUser():
    return int(input("Vui lòng nhập số thứ tự file muốn sửa hoặc quay lại:"))

def deleteNote(fileName):
    filePath = 'file/' + fileName

    os.remove(filePath)

    m.main()

def deleteNoteMain():
    print()
    print("***** MÀN HÌNH XÓA GHI CHÚ *****")

    hp.menu()
    hp.space()

    hp.showList()

    files = fileNote()
    while True:
        try:
            choose = chooseUser()
        except:
            continue
        if choose < 0 or choose > len(files):
            continue
        else:
            break
    
    if choose == 0:
        m.main()
    else:
        deleteNote(files[choose - 1])
    

