import numpy as np
import list_note as ln
import helper as hp
import os
def fileNote():
    files = os.listdir('file/')

    return files

def menu():
    print("* Tạo ghi chú mới nhập: 1")
    if len(fileNote()) > 0:
        print("* Sửa ghi chú nhập: 2")
        print("* Xóa ghi chú nhập: 3")

def showList():
    if len(fileNote()) > 0:
        print("________ DANH SÁCH GHI CHÚ ________")
        print()
    ln.showList()
    print()

def inputUser():
    return int(input("Vui lòng nhập lựa chọn của bạn:"))

def chooseUser():
    choose = 0

    chooseArr = [1, 2, 3]

    if len(fileNote()) == 0:
        chooseArr = [1]
    
    while choose not in chooseArr:
        try:
            choose = inputUser()
        except:
            continue
    
    return choose

def dashboardMain():
    menu()
    hp.space()
    showList()
    print()
    choose = chooseUser()

    return choose