import os
import list_note as ln

def inputContent():
    return input("Nhập nội dung cần ghi chú:")

def space():
    print('-----------------------------------')

def showList():
    print("________ DANH SÁCH GHI CHÚ ________")
    print()
    ln.showList()
    print()

def fileNote():
    files = os.listdir('file/')
    return files

def menu():
    print("* Quay lại nhập: 0")

def readNote(count, fileName):
    f = open('file/' + fileName, "r")
    print(str(count) + ", " + f.read())
