import dashboard as db
import add_note as an
import edit_note as en
import delete_note as dn
import numpy as np

def main():
    print()
    print("************* GHI CHÚ *************")
    print()
    choose = db.dashboardMain()

    if choose == 1:
        an.addNoteMain()
    elif choose == 2:
        en.editNoteMain()
    else:
        dn.deleteNoteMain()

if __name__ == '__main__':
    main()
